<?php
extract(shortcode_atts(array(
    'el_class'        => '',
    'bg_image'        => '',
    'bg_color'        => '',
    'bg_image_repeat' => '',
    'font_color'      => '',
    'padding'         => '',
    'margin_bottom'   => '',
    'row_id'=>''
), $atts));

$data_parallax ="";
if($parallax_class == "yes"){
    $data_parallax='data-stellar-vertical-offset="" data-stellar-background-ratio="0.6"';
}
$class_effect="";
if($data_effect !=""){
    $class_effect = " wow ".$data_effect." ";
}
wp_enqueue_style( 'js_composer_front' );
wp_enqueue_script( 'wpb_composer_front_js' );
wp_enqueue_style('js_composer_custom_css');

$el_class = $this->getExtraClass($el_class);


$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, ' st position-rel' . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);


$output .= '<div id="'.$row_id.'" class="'.$css_class.$class_effect.'"'.$style.' '.$data_parallax.'  >';



if($row_fullwidth == "yes")
{
    $output.="<div class='container-fluid' > ";

}else
{
    $output.="<div class='container'>";
}


if($overlay_class =="yes"){

    $output.='<div class="parallax-overlay"></div>';

}

$output.="<div class='row'>";

$output .= wpb_js_remove_wpautop($content);

$output.="</div><!--End .row-->";

$output.="</div><!--End .container-->";




$output .= '</div>';


$output.$this->endBlockComment('row');

$st_row_fullwidth=0;

echo $output;