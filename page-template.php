<?php 
/**
 * Template Name: Blog Page
 * 
 *
 * @package wpace framework
 */
// Đây là trang custom template google search 'page template' 
get_header();

		while (have_posts()):the_post(); 
			the_content();
		endwhile;

get_footer(); ?>