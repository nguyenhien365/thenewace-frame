<?php
/**
 * List all hooked functions
 *
 * @version 1.0
 *
 * @date 30.01.2015
 *
 * @author wpace
 *
 * */
if(!function_exists('ace_widgets_init')){
    /**
     * Register widget area.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_sidebar
     */
    function ace_widgets_init() {
        $arg1= array(
            'name'          => __( 'Blog sidebar', $ace_textdomain ),
            'id'            => 'sidebar-1',
            'description'   => '',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h1 class="widget-title">',
            'after_title'   => '</h1>',
        );
        $arg2=array(
            'name'          => __( 'Shop sidebar', $ace_textdomain ),
            'id'            => 'sidebar-2',
            'description'   => '',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h1 class="widget-title">',
            'after_title'   => '</h1>',
        );
        register_sidebar($arg1);
        register_sidebar($arg2);
    }
}
//  Gọi các file css cần dùng 
if(!function_exists('ace_styles_register')){
    function ace_styles_register()
    {
        // example for css
        wp_enqueue_style('style',get_stylesheet_uri()); // đây là file style.css mặc định phải có ngoài thư mục gốc
        wp_enqueue_style('demo',get_template_directory_uri().'/css/demo.css'); // css thêm vào theo mẫu này
        wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.min.css'); // css thêm vào theo mẫu này

    }
}
// action này sẽ trả về  script cho footer nhé thằng cờ hó 
if(!function_exists('ace_scripts_register')){

    function ace_scripts_register(){

        // example for js

        wp_enqueue_script( 'jquery', get_template_directory_uri().'/js/jquery.js', array(),'', true );

    }
}
// add ajax for theme 
if(!function_exists('ace_add_url_ajax')){
 function ace_add_url_ajax()
 {
    ?>
    <script type="text/javascript">
        var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    </script>
    <?php
 }
}
if(!function_exists('ace_menu_register')) {
   function ace_menu_register(){
        register_nav_menus( array(
            'primary' => __( 'Menu Top', $ace_textdomain ),
            'footer' => __( 'Footer Menu', $ace_textdomain ),
        ) );
   }
}
if(!function_exists('create_post_type')) {
    function create_post_type() {
      register_post_type( 'acme_product',
        array(
          'labels' => array(
            'name' => __( 'Products' ),
            'singular_name' => __( 'Product' )
          ),
          'public' => true,
          'has_archive' => true,
        )
      );
    }
}
