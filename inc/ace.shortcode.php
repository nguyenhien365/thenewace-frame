<?php 

if(class_exists('ACEBaseShortcode'))return;
class ACEBaseShortcode
{

    function __construct()
    {
        $className=get_class($this);

        $className=mb_strtolower($className);

        add_shortcode($className,array($this,'content'));

    }

    function content($attrs,$content=false)
    {

    }

}