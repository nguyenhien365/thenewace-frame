<?php
/**
 * Meta Boxes
 * creat by wpace
 */
/**
 * Initialize the meta boxes. 
 */
add_action( 'admin_init', 'custom_meta_boxes' );

function custom_meta_boxes() {

  $my_meta_box_post = array(
    'id'        => 'my_meta_box',
    'title'     => 'My Meta Box Post',
    'desc'      => '',
    'pages'     => array( 'post' ),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
      array(
        'id'          => 'background',
        'label'       => 'Background',
        'desc'        => '',
        'std'         => '',
        'type'        => 'background',
        'class'       => '',
        'choices'     => array()
      )
    )
  );
  if (function_exists('custom_meta_boxes')) {
    if (function_exists('ot_register_meta_box')) {
        ot_register_meta_box( $my_meta_box_post );        
    }
  }
}