<?php
global $st_textdomain;
vc_map( array(
    "name" => __("TS Call To Action" ,$st_textdomain),
    "base" => "st_callaction",
    "class" => "",
    "category" => 'Content',
    "icon" => "icon-st",
    "params" => array(
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Text Action", $st_textdomain),
            "param_name" => "content",
            "value" => "",
            "description" => __("Text Call To Action.", $st_textdomain)
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Sub Text Action", $st_textdomain),
            "param_name" => "st_sub_text_action",
            "value" => "",
        ),
        array(
            "type" => "vc_link",
            "holder" => "div",
            "class" => "",
            "heading" => __("Button Url", $st_textdomain),
            "param_name" => "button_url",
            "value" => "",
            "description" => "",
            'edit_field_class'=>' vc_col-sm-6',
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Position", $st_textdomain),
            "param_name" => "position",
            "value" => array("Center"=>'center',"Left"=>'left',"Right"=>'right'),
            "description" => "",
            'edit_field_class'=>' vc_col-sm-6',
        ),
    )
));

class st_callaction extends  ACEBaseShortcode
{

    function __construct()
    {
        parent::__construct();
    }

    function content($arg,$content=false)
    {
        $data = shortcode_atts(array(
            'st_sub_text_action' =>"",
            'button_url' =>"",
            'position' =>"",
        ), $arg,'st_callaction');
        array_push($data ,$content);
        extract($data);
        $txt='<div class="templatemo-welcome" id="templatemo-welcome">
            <div class="container">
                <div class="templatemo-slogan text-center">
                    <span class="txt_darkgrey">Welcome to </span><span class="txt_orange">Urbanic Design</span>
                    <p class="txt_slogan"><i>Lorem ipsum dolor sit amet, consect adipiscing elit. Etiam metus libero mauriec ignissim fermentum.</i></p>
                </div>  
            </div>
        </div>';
        return $txt;
    }
}
new st_callaction();