<?php
add_action('init','ace_menu_register'); /// thêm mới menu

add_action( 'wp_enqueue_scripts', 'ace_styles_register' ); // thêm mới style

add_action( 'wp_enqueue_scripts', 'ace_scripts_register' ); // thêm mới script

add_action( 'widgets_init', 'ace_widgets_init' ); // thêm mới widget

add_action('wp_footer', 'ace_add_url_ajax'); // require ajax 

add_action( 'init', 'create_post_type' ); // tạo mới custom post type
