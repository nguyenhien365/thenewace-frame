<?php
/**
 * Setup Class
 *
 * @package ACE Framework
 *
 * @version 1.0
 *
 * @date 29.01.2015
 *
 * 
 *
 * */
/// file này sẽ gọi tất cả các file cấu hình liên quan đến theme , các plugin cần thiết đc gọi vào , thêm menu , custom post type , add support theme , thêm css , js ...... 
define('ACE_CORE_DIR_NAME','inc');
define('ACE_CORE_DIR',get_template_directory().'/inc');
define('ACE_CORE_URI',get_template_directory_uri().'/inc');
define('ACE_LOG',true);
class ACESetup
{

    static $detect;
    static function  init()
    {

        if(class_exists('Vc_Manager'))
        {
            add_action('init',array('ACESetup','loadVcElements'));
        }
    }
	static function load_libs($files=array()){ // load  các file setup vào 
        if(!empty($files))
        {
            foreach ($files as  $value) {
                $file=ACE_CORE_DIR.'/'.$value.'.php';

                if(file_exists($file)){

                    require_once $file;
                }else{
                    echo '<pre>';
                        echo "can not load".$file;
                    echo '</pre>';
                }
            }
        }
    }
    static function loadVcElements(){
        $dirs = array_filter(glob(ACE_CORE_DIR.'/vc-elements/*'), 'is_dir');
        if(!empty($dirs))
        {
            foreach($dirs as $key=>$value)
            {

                if(is_file($value.'/'.$dirname.'.php')){
                    require_once $value.'/'.$dirname.'.php';
                }

            }
        }
    }
}
$load_file=array( 
    'ace.register',
    'ace.action',
    'ace.require_plugin',
    'ace.themeoption',
    'ace.navmenu',
    'ace.metabox',
    'ace.widget',
    'ace.shortcode',
    'ace.theme_support'

);
ACESetup::load_libs($load_file);
//ACESetup::init(); // chỉ dành cho theme sử dụng visual composer



