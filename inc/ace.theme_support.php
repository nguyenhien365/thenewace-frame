<?php
	add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
    ) );
    add_theme_support('post-thumbnails'); 
    add_theme_support( 'post-formats', array(

        'gallery', 'image', 'video', 'quote','audio',

    ) );