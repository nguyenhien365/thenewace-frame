<?php
/**
 * Initialize the options before anything else. 
 */
add_action( 'admin_init', 'cuaceom_theme_options', 1 );

/**
 * Build the cuaceom settings & update OptionTree.
 */
function cuaceom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  /**
   * Cuaceom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
 $cuaceom_settings = array(
    'contextual_help' => array(
      'content'       => array( 
        array(
          'id'        => '',
          'title'     => __('General',$ace_textdomain),
          'content'   => __('<p>Help content goes here!</p>',$ace_textdomain)
        )
      ),
      'sidebar'       => __('<p>Sidebar content goes here!</p>',$ace_textdomain),
    ),
    'sections'        => array(
          array(
            'id'          => 'logo_setting',
            'title'       => __('Logo settings',$ace_textdomain)
          ),
          array(
            'id'          => 'home_setting',
            'title'       => __('Homepage settings',$ace_textdomain)
          ),
          array(
            'id'          => 'styling_setting',
            'title'       => __('Styling settings',$ace_textdomain)
          ),

          array(
            'id'          => 'genaral_setting',
            'title'       => __('General settings',$ace_textdomain)
          ),
          array(
            'id'          => 'blog_setting',
            'title'       => __('Blog settings',$ace_textdomain)
          ),
          array(
            'id'          => 'footer_setting',
            'title'       => __('Footer settings',$ace_textdomain)
          ),
         array(
            'id'          => 'seo_setting',
            'title'       => __('Seo settings',$ace_textdomain)
          ),
    ),
    /* logo setting */
        'settings'        => array(
          array(
            'id'          => 'ace_logo',
            'label'       => __('Logo',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'upload',
            'section'     => 'logo_setting',
            'class'       => '',
          ),
    /* end logo setting *./
    /* Home setting */
          array(
            'id'          => 'gallery_home',
            'label'       => __('Home Slider',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'slider',
            'section'     => 'home_setting',
            'class'       => '',
          ),
    /* end logo setting *./
    /* seo setting */
        array(
            'id'          => 'ace_seo_phone',
            'label'       => __('Phone number',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'text',
            'section'     => 'seo_setting',
            'class'       => '',
        ),
         array(
            'id'          => 'ace_seo_mail',
            'label'       => __('Mail ',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'text',
            'section'     => 'seo_setting',
            'class'       => '',
        ),
     /* end seo setting */
    /* styling_setting setting */
        array(
        'id'          => 'ace_right_to_left',
        'label'       => __('Right to left',$ace_textdomain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'styling_setting',
        'class'       => '',
      ),
        array(
        'id'          => 'ace_font_body',
        'label'       => __('Body font',$ace_textdomain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'styling_setting',
        'class'       => '',
      ),

      array(
        'id'          => 'ace_main_color',
        'label'       => __('Main color',$ace_textdomain),
        'desc'        => __('Color default : #ff6f40',$ace_textdomain),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'styling_setting',
        'class'       => '',
        ),
      array(
        'id'          => 'ace_css_code',
        'label'       => __('Css',$ace_textdomain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'css',
        'section'     => 'styling_setting',
        'class'       => '',
        ),
    /* end styling_setting*/////////////////////////////////////////////////////////
    /* genaral_setting  aceart genaral_setting */
      array(
        'id'          => 'ace_check_preload',
        'label'       => __('Preload setting',$ace_textdomain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'genaral_setting',
        'class'       => '',
        ),
        array(
            'id'          => 'ace_load_image',
            'label'       => __('Loading image',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'upload',
            'section'     => 'genaral_setting',
            'class'       => '',
            'condition'   => 'ace_check_preload:is(on)',
        ),
        array(
            'id'          => 'ace_check_boxed',
            'label'       => __('Boxed setting',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'on-off',
            'section'     => 'genaral_setting',
            'class'       => '',
        ),
        array(
            'id'          => 'ace_bg_boxed_body',
            'label'       => __('Boxed : body background',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'background',
            'section'     => 'genaral_setting',
            'class'       => 'col-md-4',
            'condition'   => 'ace_check_boxed:is(on)',
        ),
        array(
            'id'          => 'ace_bg_boxed_content',
            'label'       => __('Boxed : content background',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'background',
            'section'     => 'genaral_setting',
            'class'       => 'col-md-4',
            'condition'   => 'ace_check_boxed:is(on)',
        ),
        array(
            'id'          => 'ace_genaral_scroll',
            'label'       => __('Smooth Scroll Setting',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'on-off',
            'section'     => 'genaral_setting',
            'class'       => '',
        ),


        array(
            'id'          => 'ace_track_code',
            'label'       => __('Tracking Code',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'rows'        =>'5',
            'type'        => 'textarea-simple',
            'section'     => 'genaral_setting',
            'class'       => '',
        ),

        array(
            'id'          => 'ace_space_before',
            'label'       => __('Space before',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'rows'        =>'5',
            'type'        => 'textarea-simple',
            'section'     => 'genaral_setting',
            'class'       => '',
        ),
        array(
            'id'          => 'ace_space_after',
            'label'       => __('Space after',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'rows'        =>'5',
            'type'        => 'textarea-simple',
            'section'     => 'genaral_setting',
            'class'       => '',
        ),
        array(
            'id'          => 'ace_seo_option',
            'label'       => __('SEO options',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'on-off',
            'section'     => 'genaral_setting',
            'class'       => '',
        ),
        array(
            'id'          => 'ace_seo_title',
            'label'       => __('SEO Title',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'type'        => 'text',
            'section'     => 'genaral_setting',
            'class'       => '',
              'condition'   => 'ace_seo_option:is(on)',
        ),
        array(
            'id'          => 'ace_seo_desc',
            'label'       => __('SEO Description',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'rows'        =>'5',
            'type'        => 'textarea-simple',
            'section'     => 'genaral_setting',
            'class'       => '',
              'condition'   => 'ace_seo_option:is(on)',
        ),
        array(
            'id'          => 'ace_seo_keywords',
            'label'       => __('SEO Keywords',$ace_textdomain),
            'desc'        => '',
            'std'         => '',
            'rows'        =>'5',
            'type'        => 'textarea-simple',
            'section'     => 'genaral_setting',
            'class'       => '',
            'condition'   => 'ace_seo_option:is(on)',
        ),
    /* end genaral  aceat blog_setting */
            array(
                'id'          => 'ace_sidebar_position',
                'label'       => __('Position',$ace_textdomain),
                'desc'        => '',
                'type'        => 'select',
                'section'     => 'blog_setting',
                'class'       => '',
                'choices'     => array(
                    array(
                        'value'=>'no',
                        'label'=>__('No Sidebar',$ace_textdomain),
                    ),
                    array(
                        'value'=>'left',
                        'label'=>__('Left',$ace_textdomain),
                    ),
                    array(
                        'value'=>'right',
                        'label'=>__('Right',$ace_textdomain),
                    )
                )
            ),
            array(
                'id'          => 'ace_sidebar',
                'label'       => __('Sidebar select',$ace_textdomain),
                'desc'        => '',
                'std'         => '',
                'type'        => 'sidebar-select',
                'section'     => 'blog_setting',
                'class'       => '',
                'condition'   => 'ace_sidebar_position:not(no)',
            ),
            array(
                'id'          => 'ace_blog_page',
                'label'       => __('Page select',$ace_textdomain),
                'desc'        => __('Select page show in behind blog content',$ace_textdomain),
                'std'         => '',
                'type'        => 'page-select',
                'section'     => 'blog_setting',
                'class'       => '',
               
            ),
    /* end blog  aceart footer_setting */
        array(
        'id'          => 'ace_footer_bg',
        'label'       => __('Background select',$ace_textdomain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'footer_setting',
        'class'       => '',
        ),
        array(
        'id'          => 'ace_footer_text',
        'label'       => __('Text footer',$ace_textdomain),
        'desc'        => '',
        'std'         => '',
        'rows'        =>'5',
        'type'        => 'textarea',
        'section'     => 'footer_setting',
        'class'       => '',
        ),
        array(
        'id'          => 'ace_footer_font',
        'label'       => __('Footer Typography',$ace_textdomain),
        'desc'        => '',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'footer_setting',
        'class'       => '',
      ),
    )
  );
  /* settings are not the same update the DB */
  if ( $saved_settings !== $cuaceom_settings ) {
    update_option( 'option_tree_settings', $cuaceom_settings ); 
  }
  
}