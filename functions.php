<?php 
/**
 * wpace framework functions and definitions
 *
 * @package wpace Framework
 *
 * @version 1.0
 * @author nguyenhien365@gmail.com skype: nguyenan0178 phonenumber: +84 988 625 017
 * @date 24.01.2015
 */
define ($ace_textdomain,$ace_textdomain);
load_theme_textdomain( $ace_textdomain, get_template_directory() . '/languages' );
// $ace_textdomain là biến sau này sẽ dùng để dịch theme sang ngôn ngữ khác nói chung quan tâm thằng dưới này thôi
if(file_exists(get_template_directory().'/inc/ace.setup.php'))
{
    require_once get_template_directory().'/inc/ace.setup.php';
}
// tìm sang file ace.setup.php trong thư mục để xem tiếp 
