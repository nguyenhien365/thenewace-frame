## How to use thenewace worpress framework
### Step 1 :
    * cài đặt wp
    * Đưa thư mục theme thenewace vào như các theme khác 
    * active theme
    * vào phần plugin nhấp vào require plugin cài đặt các plugin cần thiết
### Step 2 :
    * Copy các file cần thiết thư image, css , js vào thư mục tương ứng trong 
    theme thenewace
### Step 3 :
    * Cấu trúc thư mục cài đặt : Cấu hình cài đặt được nằm trong file inc/ace.setup.php. Các thư viện , css, js được khai báo ở đây . File ace.setup sẽ load tất cả các file cài đặt vào . Dưới đây là tác dụng các file
    * ace.action.php : chạy các hàm trong file ace.register.php 
    * ace.metabox.php : tạo các metabox bằng plugin theme option tree (là các thành phần thay đổi , dành riêng cho từng loại page , post , custom post )
    * ace.navmenu.php : chỉnh sửa các class , attribute cho menu 
    * ace.register.php : Khai báo các script , css dùng trong trang . tạo mới menu , widget cụ thể xem comment trong file
    * ace.shortcode.php : thêm shortcode
    * ace.theme_suport.php : add suport for theme
    * ace.themeoption.php : tạo các themeoption cho web (là các thành phần không thay đổi trong trang như logo , title , decription , slide trang chủ ....) sử dụng plugin option tree [Xem thêm](https://wordpress.org/plugins/option-tree)
    * ace.widget.php : tạo các widget 
###### thanks for use thenewace framework 
    - contact - nguyenhien365@gmail.com or skype nguyenan0178 *
    - Thewpace team 
    
    